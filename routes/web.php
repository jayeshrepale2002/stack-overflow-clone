<?php

use App\Http\Controllers\AnswersController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\FavouritesController;
use App\Http\Controllers\VotesController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/questions', QuestionsController::class)->except('show');
Route::get('/questions/{slug}', [QuestionsController::class, 'show'])->name('questions.show');
Route::resource('questions.answer', AnswersController::class)->except(['create', 'show']);
Route::put('/questions/{question}/answers/{answer}/mark-as-best', [AnswersController::class, 'markAsBest'])->name('questions.answer.markAsBest');
Route::post('/questions/{question}/mark-as-fav', [FavouritesController::class, 'store'])->name('questions.mark-as-fav');

Route::delete('/questions/{question}/mark-as-unfav', [FavouritesController::class, 'destroy'])->name('questions.mark-as-unfav');

Route::post('/questions/{question}/vote/{vote}', [VotesController::class, 'voteQuestion'])->name('questions.vote');

Route::post('/questions/{question}/answers/{answer}/vote/{vote}', [VotesController::class, 'voteAnswer'])->name('answers.vote');
