<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        User::create([
            'name' => 'Jayesh Repale',
            'email' => 'jayeshrepale2002@gmail.com',
            'password' => Hash::make('Jayesh1234')
        ]);

        \App\Models\User::factory(10)->create()->each(function($user) {
            $user->questions()->saveMany(
                Question::factory(random_int(5, 15))->make()
            )->each(function($question) {
                $question->answer()->saveMany(
                    Answer::factory(random_int(2, 10))->make()
                );
            });
        });

        // Here we are creating multiple questions for each user and create() returns object of collection.
        // each() is a method of collection which acts as a loop.

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
